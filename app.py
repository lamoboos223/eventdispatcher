import pyodbc
from flask import Flask, request
import requests


app = Flask(__name__)


@app.route("/sendEvent", methods=['POST'])
def send_event():
    response = request.get_json()
    employee_name = response.get('name')
    companies = get_companies()

    for company in companies:
        company_name = company[0]
        webhook = company[1]
        body = "Dear company %s i'm %s i would like to join your company." %(company_name, employee_name)
        requests.post(url =webhook, data = body)

    return employee_name



def get_companies():
    # Create connection
    con = pyodbc.connect(driver="{SQL Server}",server='localhost,1434',database='EventDispatcher',uid='',pwd='')
    cur = con.cursor()
    db_cmd = "SELECT * FROM dbo.Company"
    companies = cur.execute(db_cmd)

    return companies


if __name__ == '__main__':
    app.run(debug=True,
            port=8080,
            host="127.0.0.1")
